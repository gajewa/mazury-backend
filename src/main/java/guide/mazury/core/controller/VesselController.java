package guide.mazury.core.controller;

import guide.mazury.core.dto.NewVessel;
import guide.mazury.core.entity.Vessel;
import guide.mazury.core.service.AuthorizationService;
import guide.mazury.core.service.VesselService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/vessels")
@RequiredArgsConstructor
public class VesselController {

    private final VesselService vesselService;
    private final AuthorizationService authorizationService;

    @GetMapping("/{id}")
    public Vessel create(@PathVariable Long id) {
        return vesselService.findById(id);
    }

    @DeleteMapping("/{vesselId}")
    public void delete(@PathVariable Long vesselId) {
        vesselService.delete(vesselId);
    }

    @PostMapping
    @PreAuthorize("@authorizationService.isAdminOfLocation(#authentication, #vessel.locationId)")
    public Vessel create(@RequestBody NewVessel vessel, Authentication authentication) {
        return vesselService.create(vessel);
    }

    @PutMapping("/{id}")
    @PreAuthorize("@authorizationService.isAdminOfVessel(#authentication, #id)")
    public Vessel updateById(@PathVariable Long id, @RequestBody Vessel vessel, Authentication authentication) {
        return vesselService.updateById(id, vessel);
    }

}
