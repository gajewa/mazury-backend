package guide.mazury.core.controller;

import guide.mazury.core.dto.ContactEmailForm;
import guide.mazury.core.dto.LocationFilters;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.Vessel;
import guide.mazury.core.service.AuthorizationService;
import guide.mazury.core.service.LocationService;
import guide.mazury.core.service.VesselService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/locations")
@RequiredArgsConstructor
public class LocationController {

    private final LocationService locationService;
    private final VesselService vesselService;
    private final AuthorizationService authorizationService;

    @GetMapping
    public List<Location> findAll(LocationFilters locationFilters) {
        return locationService.findAll(locationFilters);
    }

    @GetMapping("/{id}")
    public Location findById(@PathVariable Long id) {
        return locationService.findById(id);
    }

    @GetMapping("/slug/{slug}")
    public Location findBySlug(@PathVariable String slug) {
        return locationService.findBySlug(slug);
    }

    @GetMapping("/{locationSlug}/vessels")
    public List<Vessel> findVesselsForLocation(@PathVariable String locationSlug) {
        return vesselService.findByLocation(locationSlug);
    }

    @GetMapping("/{locationSlug}/vessels/{id}")
    public Vessel findVesselsForLocationAndId(@PathVariable String locationSlug, @PathVariable Long id) {
        return vesselService.findByLocationAndId(locationSlug, id);
    }
    
    @PostMapping("/{locationId}/mail")
    public void sendContactMail(@PathVariable Long locationId, @RequestBody ContactEmailForm contactEmailForm){
        locationService.sendContactEmail(locationId, contactEmailForm);
    }

    @PutMapping("/{id}/general-info")
    @PreAuthorize("@authorizationService.isAdminOfLocation(#authentication, #id)")
    public Location updateGeneralInfoById(@PathVariable Long id, @RequestBody Location location, Authentication authentication) {
        return locationService.updateGeneralInfoById(id, location);
    }

    @PutMapping("/{id}/port-info")
    @PreAuthorize("@authorizationService.isAdminOfLocation(#authentication, #id)")
    public Location updatePortInfoById(@PathVariable Long id, @RequestBody Location location, Authentication authentication) {
        return locationService.updatePortInfoById(id, location);
    }

}
