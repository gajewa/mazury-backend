package guide.mazury.core.controller;

import guide.mazury.core.dto.ErrorMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;


@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class ExceptionHandlers {

    @ExceptionHandler
    public ResponseEntity<Object> handleIllegalArgumentException(Exception e) {
        String errorId = RandomStringUtils.randomAlphanumeric(8);
        log.error("{} Error identifier: {} {}", e.getMessage(), errorId, e);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);

        ErrorMessage errorMessage = ErrorMessage.builder()
                .dateTime(LocalDateTime.now())
                .message(e.getMessage())
                .errorIdentifier(errorId)
                .stackTrace(sw.toString())
                .build();

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(errorMessage);
    }
}



