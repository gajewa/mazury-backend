package guide.mazury.core.controller;

import guide.mazury.core.dto.EmailAndPassword;
import guide.mazury.core.dto.UserLoggedInDTO;
import guide.mazury.core.entity.AvailableAuth;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.User;
import guide.mazury.core.enums.AuthenticationSource;
import guide.mazury.core.repository.AvailableAuthRepository;
import guide.mazury.core.repository.UserRepository;
import guide.mazury.core.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;
    private final AvailableAuthRepository availableAuthRepository;

    @GetMapping("/availableAuths")
    public ResponseEntity getAvailableAuths(Model model) {
        List<AvailableAuth> availableAuths = availableAuthRepository.findAllByActiveTrue();

        return ResponseEntity.status(401).body(availableAuths);
    }

    @PostMapping("/login")
    public UserLoggedInDTO login(@RequestBody EmailAndPassword emailAndPassword) {
        String token = userService.login(emailAndPassword);

        return new UserLoggedInDTO(token);
    }

    @PostMapping("/register")
    public void register(@RequestBody EmailAndPassword emailAndPassword) {
        userService.register(emailAndPassword);
    }

    @PostMapping("/confirm")
    public void confirm(@RequestParam(required = false) String token, @RequestParam(required = false) String email) {
        userService.confirmUser(email, token);
    }

    @GetMapping("/{id}/locations")
    public Set<Location> getUsersLocations(@PathVariable Long id) {
        User user = userService.findById(id);
        return user.getLocations();
    }

    @GetMapping("/current")
    public User currentUser(Authentication authentication) {
        User user;
        if (authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthenticationToken oAuth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;

//            String externalId = isGoogleAuthenticated(oAuth2AuthenticationToken)
//                    ? oAuth2AuthenticationToken.getPrincipal().getAttributes().get("sub").toString()
//                    : oAuth2AuthenticationToken.getPrincipal().getAttributes().get("id").toString();
            
            String email = oAuth2AuthenticationToken.getPrincipal().getAttributes().get("email").toString();
            AuthenticationSource source= isGoogleAuthenticated(oAuth2AuthenticationToken) ? AuthenticationSource.GOOGLE : AuthenticationSource.FACEBOOK;

            user = userRepository.findByEmail(email)
                    .orElseGet(() -> {
                        Map attributes = oAuth2AuthenticationToken.getPrincipal().getAttributes();
                        log.info("User {} logged in for the first time.", attributes.get("name").toString());
                        return userRepository.save(
                                User.builder()
                                        .name(attributes.get("name").toString())
                                        .email(attributes.get("email").toString())
                                        .authenticationSource(source)
//                                        .externalId(externalId)
                                        .build()
                        );
                    });
        } else {
            String email = authentication.getPrincipal().toString();
            user = userService.findByEmail(email);
        }

        return user;
    }

    @GetMapping("/loginSuccess")
    public User getLoginInfo(Model model, OAuth2AuthenticationToken authentication) {

        String externalId = isGoogleAuthenticated(authentication)
                ? authentication.getPrincipal().getAttributes().get("sub").toString()
                : authentication.getPrincipal().getAttributes().get("id").toString();

        User user = userRepository.findByExternalId(externalId)
                .orElseGet(() -> {
                    Map attributes = authentication.getPrincipal().getAttributes();

                    return userRepository.save(
                            User.builder()
                                    .name(attributes.get("name").toString())
                                    .authenticationSource(isGoogleAuthenticated(authentication) ? AuthenticationSource.GOOGLE : AuthenticationSource.FACEBOOK)
                                    .externalId(externalId)
                                    .build()
                    );
                });

        return user;
    }

    private boolean isGoogleAuthenticated(OAuth2AuthenticationToken authentication) {
        return authentication.getAuthorizedClientRegistrationId().equals("google");
    }

}
