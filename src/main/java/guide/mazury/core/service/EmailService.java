package guide.mazury.core.service;

import guide.mazury.core.dto.ContactEmailForm;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService {

    private final JavaMailSender emailSender;

    @SneakyThrows
    public void sendContactEmail(Location location, ContactEmailForm contactEmailForm) {
        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        String emailContent = getTemplateContent("emailMessageTemplate.html");
        emailContent = emailContent.replace("{name}", contactEmailForm.getName());
        emailContent = emailContent.replace("{email}", contactEmailForm.getEmail());
        emailContent = emailContent.replace("{content}", contactEmailForm.getContent());
        emailContent = emailContent.replace("{phoneNumber}", contactEmailForm.getPhoneNumber());

        helper.setText(emailContent, true);
        helper.setTo(location.getEmail());
        helper.setSubject(String.format("Nowe zapytanie z serwisu mazury.guide od %s", contactEmailForm.getName()));
        helper.setFrom("hello@mazury.guide");

        emailSender.send(mimeMessage);
    }

    @SneakyThrows
    public void sendVerificationEmail(User user) {
        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        String emailContent = getTemplateContent("verificationMessageTemplate.html");
        emailContent = emailContent.replace("{email}", user.getEmail());
        emailContent = emailContent.replace("{token}", user.getConfirmationToken());

        helper.setText(emailContent, true);
        helper.setTo(user.getEmail());
        helper.setSubject("Potwierdznie adresu email w mazury.guide");
        helper.setFrom("hello@mazury.guide");

        try {
            emailSender.send(mimeMessage);
        } catch (Exception e) {
            throw new RuntimeException("Nie udało się wysłać maila z potwierdzeniem. Czy adres email jest prawidłowy?");
        }
    }

    private String getTemplateContent(String templateName) {
        String content = StringUtils.EMPTY;
        try {
            InputStream templateInputStream = new ClassPathResource(templateName).getInputStream();
            byte[] templateAsBytes = IOUtils.toByteArray(templateInputStream);
            content = new String(templateAsBytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.warn("File Not Found. Message: {}", e.getMessage());
        }
        return content;
    }
}
