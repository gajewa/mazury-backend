package guide.mazury.core.service;

import guide.mazury.core.component.Authenticator;
import guide.mazury.core.component.TokenIssuer;
import guide.mazury.core.dto.EmailAndPassword;
import guide.mazury.core.entity.User;
import guide.mazury.core.enums.AuthenticationSource;
import guide.mazury.core.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;


@Service
@RequiredArgsConstructor
public class UserService {

    private final Authenticator authenticator;
    private final TokenIssuer tokenIssuer;
    private final UserRepository userRepository;
    private final EmailService emailService;

    public String login(EmailAndPassword emailAndPassword) {
        User authenticatedUser = authenticator.authenticate(emailAndPassword);

        return tokenIssuer.issueToken(authenticatedUser);
    }

    @Transactional
    public void register(EmailAndPassword emailAndPassword) {
        if (userRepository.existsByEmail(emailAndPassword.getEmail())){
            throw new RuntimeException("Użytkownik o takim adresie email już istnieje.");
        }
        
        String hashedPassword = DigestUtils.sha512Hex(emailAndPassword.getPassword());

        User user = User.builder()
                .email(emailAndPassword.getEmail())
                .password(hashedPassword)
                .authenticationSource(AuthenticationSource.INTERNAL)
                .expirationTimeInMinutes(2880L)
                .name(emailAndPassword.getName())
                .confirmationToken(RandomStringUtils.randomAlphanumeric(25))
                .build();
        
        userRepository.save(user);

        emailService.sendVerificationEmail(user);
    }

    public void confirmUser(String email, String token) {
        User userToConfirm = userRepository.findByConfirmationTokenAndEmail(token, email)
                .orElseThrow(() -> new EntityNotFoundException("User not found for email " + email + " and cofirmation token " + token));

        userToConfirm.setConfirmed(true);
        userRepository.save(userToConfirm);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found with ID " + id));
    }

    public User findByExternalId(String externalId) {
        return userRepository.findByExternalId(externalId)
                .orElseThrow(() -> new EntityNotFoundException("User not found with externalId " + externalId));
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User not found with email " + email));
    }
}
