package guide.mazury.core.service;

import guide.mazury.core.dto.LocationFilters;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.User;
import guide.mazury.core.repository.LocationRepository;
import guide.mazury.core.repository.SpecificationBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorizationService {

    private final UserService userService;

    public boolean isAdminOfLocation(Authentication authentication, Long locationId) {
        log.info("Method isAdminOfLocation invoked [auth2AuthenticationToken={}, locationId={}]", authentication, locationId);
        User user;
        String email;
        if (authentication instanceof OAuth2AuthenticationToken) {
            // oauth login
            email = ((OAuth2AuthenticationToken) authentication).getPrincipal().getAttributes().get("email").toString();
        } else {
            // internal login
            email = authentication.getPrincipal().toString();
        }
        
        user = userService.findByEmail(email);
        boolean isAdmin = user.getLocations().stream().anyMatch(l -> l.getId().equals(locationId));

        if (!isAdmin) {
            log.info("User with auth {} tried to update location {} despite not being the admin.", authentication, locationId);
        }
        
        return isAdmin;
    }

    public boolean isAdminOfVessel(Authentication authentication, Long vesselId) {
        log.info("Method isAdminOfLocation invoked [auth2AuthenticationToken={}, vesselId={}]", authentication, vesselId);
        User user;
        String email;
        if (authentication instanceof OAuth2AuthenticationToken) {
            // oauth login
            email = ((OAuth2AuthenticationToken) authentication).getPrincipal().getAttributes().get("email").toString();
        } else {
            // internal login
            email = authentication.getPrincipal().toString();
        }

        user = userService.findByEmail(email);
        
        for (Location location : user.getLocations()) {
            if (location.getVessels().stream().anyMatch(vessel -> vessel.getId().equals(vesselId))) {
                return true;
            }
        }

        log.info("User with auth {} tried to update vessel {} despite not being the admin.", authentication, vesselId);
        return false;
    }


}
