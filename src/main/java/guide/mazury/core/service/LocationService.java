package guide.mazury.core.service;

import com.github.slugify.Slugify;
import guide.mazury.core.dto.ContactEmailForm;
import guide.mazury.core.dto.LocationFilters;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.StopType;
import guide.mazury.core.entity.Vessel;
import guide.mazury.core.repository.LocationRepository;
import guide.mazury.core.repository.SpecificationBuilder;
import guide.mazury.core.repository.StopTypeRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LocationService {

    private final LocationRepository locationRepository;
    private final StopTypeRepository stopTypeRepository;
    private final EmailService emailService;

    public List<Location> findAll(LocationFilters locationFilters) {
        Specification<Location> specification = SpecificationBuilder.buildSpecification(locationFilters);
        if (locationFilters.getLocationType() != null && locationFilters.getLocationType().equals(Location.LocationType.CHARTER) && isAnyVesselFilterSet(locationFilters)) {
            return locationRepository.findAll(specification)
                    .stream()
                    .peek(l -> {
                        List<Vessel> vesselsPerLocation = l.getVessels();
                        l.setVessels(vesselsPerLocation.stream()
                                .filter(v -> (locationFilters.getMinVesselCrew() == null && locationFilters.getMaxVesselCrew() == null) || v.getMaxCrew() != null)
                                .filter(v -> (locationFilters.getMinVesselLength() == null && locationFilters.getMaxVesselLength() == null) || v.getLength() != null)
                                .filter(v -> locationFilters.getMaxVesselLength() == null || v.getLength().compareTo(locationFilters.getMaxVesselLength()) < 0)
                                .filter(v -> locationFilters.getMinVesselLength() == null || v.getLength().compareTo(locationFilters.getMinVesselLength()) > 0)
                                .filter(v -> locationFilters.getMaxVesselCrew() == null || v.getMaxCrew() < locationFilters.getMaxVesselCrew())
                                .filter(v -> locationFilters.getMinVesselCrew() == null || v.getMaxCrew() > locationFilters.getMinVesselCrew())
                                .filter(v -> locationFilters.getLicenceNeededForVessel() == null || v.isLicenceNeeded() == locationFilters.getLicenceNeededForVessel())
                                .filter(v -> locationFilters.getVesselCategory() == null || v.getCategory().equals(locationFilters.getVesselCategory()))
                                .collect(Collectors.toList()));
                    })
                    .filter(loc -> !CollectionUtils.isEmpty(loc.getVessels()))
                    .collect(Collectors.toList());
        }
        return locationRepository.findAll(specification);
    }

    private boolean isAnyVesselFilterSet(LocationFilters locationFilters) {
        return locationFilters.getLicenceNeededForVessel() != null ||
                locationFilters.getMinVesselLength() != null ||
                locationFilters.getMaxVesselLength() != null ||
                locationFilters.getMaxVesselCrew() != null ||
                locationFilters.getMinVesselCrew() != null ||
                StringUtils.isNotBlank(locationFilters.getVesselType()) ||
                locationFilters.getVesselCategory() != null;
    }

    public Location findById(Long id) {
        return locationRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No location found of ID " + id));
    }

    public Location findBySlug(String slug) {
        return locationRepository.findBySlug(slug)
                .orElseThrow(() -> new EntityNotFoundException("No location found of slug " + slug));
    }

    public Location updateGeneralInfoById(Long id, Location location) {
        Location locationToUpdate = findById(id);
        locationToUpdate.setName(location.getName());
        locationToUpdate.setSlug(new Slugify().slugify(location.getName()));
        locationToUpdate.setPhoneNumber(location.getPhoneNumber());
        locationToUpdate.setEmail(location.getEmail());
        locationToUpdate.setWebsite(location.getWebsite());
        locationToUpdate.setDescription(location.getDescription());
        locationToUpdate.setContactFormActive(location.isContactFormActive());
        return locationRepository.save(locationToUpdate);
    }

    public Location updatePortInfoById(Long id, Location location) {
        Location locationToUpdate = findById(id);
        locationToUpdate.setDepthMax(location.getDepthMax());
        locationToUpdate.setDepthMin(location.getDepthMin());
        locationToUpdate.setPricePerPerson(location.getPricePerPerson());
        locationToUpdate.setPricePerVessel(location.getPricePerVessel());

        Set<StopType> stopTypesToSet = location.getStopTypes()
                .stream()
                .map(stopType -> stopTypeRepository.findByName(stopType.getName()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());

        locationToUpdate.setStopTypes(stopTypesToSet);

        locationToUpdate.setElectricity(location.isElectricity());
        locationToUpdate.setElectricityPrice(location.getElectricityPrice());

        locationToUpdate.setBar(location.isBar());
        locationToUpdate.setBarDistance(location.getBarDistance());

        locationToUpdate.setShower(location.isShower());
        locationToUpdate.setShowerPrice(location.getShowerPrice());

        locationToUpdate.setShop(location.isShop());
        locationToUpdate.setShopDistance(location.getShopDistance());

        locationToUpdate.setToilet(location.isToilet());
        locationToUpdate.setToiletPrice(location.getToiletPrice());

        locationToUpdate.setWater(location.isWater());
        locationToUpdate.setWaterPrice(location.getWaterPrice());

        return locationRepository.save(locationToUpdate);
    }

    public void sendContactEmail(Long locationId, ContactEmailForm contactEmailForm) {
        Location location = findById(locationId);
        if (!location.isContactFormActive()) {
            throw new IllegalArgumentException("Contact form not active for location " + location.getName());
        }

        emailService.sendContactEmail(location, contactEmailForm);
    }
}
