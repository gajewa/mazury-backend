package guide.mazury.core.service;

import guide.mazury.core.dto.NewVessel;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.User;
import guide.mazury.core.entity.Vessel;
import guide.mazury.core.repository.VesselRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VesselService {

    private final VesselRepository vesselRepository;
    private final LocationService locationService;
    private final UserService userService;

    public List<Vessel> findByLocation(String locationSlug) {
        Location location = locationService.findBySlug(locationSlug);
        return vesselRepository.findAllByLocation(location);
    }

    public Vessel create(NewVessel vessel) {
        Location location = locationService.findById(vessel.getLocationId());

        Vessel vesselEntity = Vessel.builder()
                .type(vessel.getType())
                .name(vessel.getName())
                .length(vessel.getLength())
                .width(vessel.getWidth())
                .immersion(vessel.getImmersion())
                .pricePerDayMin(vessel.getPricePerDayMin())
                .pricePerDayMax(vessel.getPricePerDayMax())
                .deposit(vessel.getDeposit())
                .maxCrew(vessel.getMaxCrew())
                .licenceNeeded(vessel.isLicenceNeeded())
                .category(vessel.getCategory())
                .location(location)
                .build();

        return vesselRepository.save(vesselEntity);
    }

    public Vessel findByLocationAndId(String locationSlug, Long id) {
        locationService.findBySlug(locationSlug);
        return findById(id);
    }

    public Vessel findById(Long id) {
        return vesselRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Vessel not found for id " + id));
    }

    public Vessel updateById(Long id, Vessel vessel) {
        Vessel vesselToUpdate = findById(id);
        vesselToUpdate.setType(vessel.getType());
        vesselToUpdate.setName(vessel.getName());
        vesselToUpdate.setPricePerDayMin(vessel.getPricePerDayMin());
        vesselToUpdate.setPricePerDayMax(vessel.getPricePerDayMax());
        vesselToUpdate.setDeposit(vessel.getDeposit());
        vesselToUpdate.setLength(vessel.getLength());
        vesselToUpdate.setWidth(vessel.getWidth());
        vesselToUpdate.setImmersion(vessel.getImmersion());
        vesselToUpdate.setMaxCrew(vessel.getMaxCrew());
        vesselToUpdate.setCategory(vessel.getCategory());
        vesselToUpdate.setLicenceNeeded(vessel.isLicenceNeeded());
        return vesselRepository.save(vesselToUpdate);
    }

    public void delete(Long vesselId) {
        Vessel vessel = findById(vesselId);
        User user;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        if (authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthenticationToken authenticationToken = (OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
            String userExternalId = authenticationToken.getPrincipal().getAttributes().get("id").toString();
            user = userService.findByExternalId(userExternalId);
        } else {
            String email = authentication.getPrincipal().toString();
            user = userService.findByEmail(email);
        }

        if (!user.getLocations().contains(vessel.getLocation())) {
            String message = "User %s tried deleting vessel %s (id %s) but is not admin of location %s (%s)".formatted(user.getId(), vessel.getType(), vessel.getId(), vessel.getLocation().getName(), vessel.getLocation().getId());
            throw new AccessDeniedException(message);
        }

        vesselRepository.delete(vessel);
    }
}
