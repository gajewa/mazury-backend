package guide.mazury.core.repository;

import guide.mazury.core.dto.LocationFilters;
import guide.mazury.core.entity.Location;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UtilityClass
public class SpecificationBuilder {

    public static Specification<Location> buildSpecification(LocationFilters filters) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            isEqualTo(predicates, root, criteriaBuilder, "type", filters.getLocationType());
            contains(predicates, root, criteriaBuilder, "name", filters.getName());

            Predicate[] pred = predicates.toArray(Predicate[]::new);
            return criteriaBuilder.and(pred);
        };
    }

    private static <T> void notIn(List<Predicate> predicates, Root<Location> root, CriteriaBuilder criteriaBuilder, String attr, List<T> value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.not(root.get(attr).in(value)));
        }
    }

    private static <T> void isEqualTo(final List<Predicate> predicates, final Root<Location> root, final CriteriaBuilder criteriaBuilder, String attr, T value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.equal(root.get(attr), value));
        }
    }

    private static <T> void contains(final List<Predicate> predicates, final Root<Location> root, final CriteriaBuilder criteriaBuilder, String attr, String value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(attr)), "%" + value.toLowerCase() + "%"));
        }
    }

    private static <T> void isBefore(final List<Predicate> predicates, final Root root, final CriteriaBuilder criteriaBuilder, String attr, T value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(attr), (LocalDate) value));
        }
    }

    private static <T> void isAfter(final List<Predicate> predicates, final Root root, final CriteriaBuilder criteriaBuilder, String attr, T value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(attr), (LocalDate) value));
        }
    }

    private static Predicate isBetween(final Root root, final CriteriaBuilder criteriaBuilder, String attrFrom, String attrTo, LocalDate value) {
        return criteriaBuilder.between(criteriaBuilder.literal(value), root.get(attrFrom), root.get(attrTo));
    }

    private static <T> void isAfterOrNull(final List<Predicate> predicates, final Root root, final CriteriaBuilder criteriaBuilder, String attr, T value) {
        if (isSet(value)) {
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.greaterThanOrEqualTo(root.get(attr), (LocalDate) value),
                    criteriaBuilder.isNull(root.get(attr)))
            );

        }
    }

    private static Predicate isNull(final Root root, final CriteriaBuilder criteriaBuilder, String attr) {
        return criteriaBuilder.isNull(root.get(attr));
    }

    private static <T> boolean isSet(final T value) {
        return value != null;
    }

    private static <T> boolean isNotSet(final T value) {
        return !isSet(value);
    }
}
