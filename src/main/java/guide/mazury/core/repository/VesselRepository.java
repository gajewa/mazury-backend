package guide.mazury.core.repository;

import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.Vessel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;


public interface VesselRepository extends JpaRepository<Vessel, Long>, JpaSpecificationExecutor<Location> {
    
    List<Vessel> findAllByLocation(Location location);
    
}
