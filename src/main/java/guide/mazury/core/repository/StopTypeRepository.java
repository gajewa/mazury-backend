package guide.mazury.core.repository;

import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.StopType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface StopTypeRepository extends JpaRepository<StopType, Long> {
    
    Optional<StopType> findByName(String name);
    
}
