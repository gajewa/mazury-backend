package guide.mazury.core.repository;

import guide.mazury.core.entity.AvailableAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AvailableAuthRepository extends JpaRepository<AvailableAuth, Long> {

    public List<AvailableAuth> findAllByActiveTrue();

}
