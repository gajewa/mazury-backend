package guide.mazury.core.repository;

import guide.mazury.core.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmailAndPassword(String username, String hashedPassword);

    Optional<User> findByExternalId(String externalId);

    Optional<User> findByEmail(String externalId);
    
    Optional<User> findByConfirmationTokenAndEmail(String confirmationToken, String email);
    
    boolean existsByEmail(String email);
}
