package guide.mazury.core.repository;

import guide.mazury.core.dto.LocationFilters;
import guide.mazury.core.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface LocationRepository extends JpaRepository<Location, Long>, JpaSpecificationExecutor<Location> {

    Optional<Location> findBySlug(String slug);

    boolean existsByIdAndContactFormActiveIsTrue(Long id);

    Location findByGoogleId(String googleId);

    @Query(
            value = """
                    select distinct l
                    from Location l
                            join l.vessels v
                    where :#{#locationFilters.name} is null or lower(l.name) like concat('%',lower(:#{#locationFilters.name}),'%')
                    and :#{#locationFilters.maxVesselLength} is null or v.length < :#{#locationFilters.maxVesselLength}
                    and :#{#locationFilters.minVesselLength} is null or v.length < :#{#locationFilters.minVesselLength}
                    and :#{#locationFilters.maxVesselCrew} is null or v.maxCrew < :#{#locationFilters.maxVesselCrew}
                    and :#{#locationFilters.minVesselCrew} is null or v.maxCrew > :#{#locationFilters.minVesselCrew}
                    and :#{#locationFilters.licenceNeededForVessel} is null or v.licenceNeeded = :#{#locationFilters.licenceNeededForVessel}
                    and :#{#locationFilters.vesselCategory} is null or v.category = :#{#locationFilters.vesselCategory}                        
                    """)
    List<Location> findAllFiltered(LocationFilters locationFilters);

//    @Query(
//            value = """
//                    select distinct l
//                    from Location l 
//                    where l.type = Location.LocationType.CHARTER 
//                    and l.vessels.size > 0                   
//                    """)
//    List<Location> findAllWithVessels();

    @Query(
            value = """
                    select distinct l
                    from Location l
                    join l.vessels v
                    where :#{#locationFilters.name} is null or lower(l.name) like concat('%',lower(:#{#locationFilters.name}),'%')
                    and :#{#locationFilters.maxVesselLength} is null or v.length < :#{#locationFilters.maxVesselLength}   
                    """)
    List<Location> findAllFiltered2(LocationFilters locationFilters);

}
