package guide.mazury.core.enums;

public enum AuthenticationSource {
    FACEBOOK, GOOGLE, INTERNAL
}
