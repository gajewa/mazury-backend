package guide.mazury.core.component;

import guide.mazury.core.entity.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class TokenIssuer {

    @Value("${security.expirationTimeInMinutes}")
    Integer expirationTimeInMinutes;

    @Value("${security.issuer}")
    String issuer;

    private final KeyProvider keyProvider;

    public String issueToken(User user) {
        Key key = keyProvider.getKey();
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime expirationDateTime = currentDateTime.plusMinutes(user.getExpirationTimeInMinutes());

        return Jwts.builder()
            .setSubject(user.getEmail())
            .setIssuer(issuer)
            .setIssuedAt(Date.from(currentDateTime.atZone(ZoneId.systemDefault()).toInstant()))
            .setExpiration(Date.from(expirationDateTime.atZone(ZoneId.systemDefault()).toInstant()))
            .signWith(SignatureAlgorithm.HS512, key)
             .compact();
    }
}
