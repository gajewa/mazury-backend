package guide.mazury.core.component;

import guide.mazury.core.dto.EmailAndPassword;
import guide.mazury.core.entity.User;
import guide.mazury.core.exception.AuthenticationException;
import guide.mazury.core.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class Authenticator {

    private static final String AUTHENTICATION_ERROR_MESSAGE = "Unable to authenticate user = {0} with given password. Login or password is incorrect.";

    private final UserRepository userRepository;

    public User authenticate(EmailAndPassword emailAndPassword) {
        log.info("Authenticating user = {}.", emailAndPassword.getEmail());

        String hashedPassword = DigestUtils.sha512Hex(emailAndPassword.getPassword());

        Optional<User> user = userRepository.findByEmailAndPassword(emailAndPassword.getEmail(), hashedPassword);

        if (user.isPresent()) {
            User authenticatedUser = user.get();
            
            if (!authenticatedUser.isConfirmed()){
                log.warn("Unable to authenticate user = {}. User not confirmed!", emailAndPassword.getEmail());
                throw new AuthenticationException(MessageFormat.format("Unable to authenticate user {0}. User not confirmed!", emailAndPassword.getEmail()));
            }
            
            log.info("Authentication successful for user = {}.", emailAndPassword.getEmail());
            return authenticatedUser;
        } else {
            log.warn("Unable to authenticate user = {}. Probable wrong login or password!", emailAndPassword.getEmail());
            throw new AuthenticationException(MessageFormat.format(AUTHENTICATION_ERROR_MESSAGE, emailAndPassword.getEmail()));
        }
    }
}
