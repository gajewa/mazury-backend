package guide.mazury.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.Vessel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;


@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NewVessel {

    private String type;
    
    private String name;
    
    private BigDecimal length;

    private BigDecimal width;

    private BigDecimal immersion;

    private BigDecimal pricePerDayMin;

    private BigDecimal pricePerDayMax;

    private BigDecimal deposit;

    private Integer maxCrew;
    
    private boolean licenceNeeded;
    
    private Vessel.VesselCategory category;

    private Long locationId;
    
}
