package guide.mazury.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailAndPassword {

    private String email;

    private String password;
    
    private String name;
}
