package guide.mazury.core.dto;

import guide.mazury.core.entity.Location;
import guide.mazury.core.entity.Vessel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class LocationFilters {
    
    private Location.LocationType locationType;
    private String name; 
    
    // vessel filters
    private Boolean licenceNeededForVessel;
    private String vesselType;
    private BigDecimal maxVesselLength;
    private BigDecimal minVesselLength;
    private Integer maxVesselCrew;
    private Integer minVesselCrew;
    private Vessel.VesselCategory vesselCategory;
}
