package guide.mazury.core.dto;

import lombok.Data;

@Data
public class ContactEmailForm {
    
    private String name;
    
    private String email;
    
    private String phoneNumber;
    
    private String content;
    
}
