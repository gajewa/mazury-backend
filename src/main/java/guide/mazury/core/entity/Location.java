package guide.mazury.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Location extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String slug;

    private String website;

    private String websiteLabel;
    
    private String email;
    
    private String phoneNumber;

    private String lat;

    private String lang;

    private String locationLake;

    private String locationCity;

    private String locationStreet;

    private String kind;

    private String description;

    @Enumerated(EnumType.STRING)
    private LocationType type;

    private BigDecimal depthMin;

    private BigDecimal depthMax;

    private BigDecimal pricePerVessel;

    private BigDecimal pricePerPerson;

    private boolean bar;

    private Integer barDistance;

    private boolean electricity;

    private BigDecimal electricityPrice;

    private boolean shower;

    private BigDecimal showerPrice;

    private boolean shop;

    private Integer shopDistance;

    private boolean toilet;

    private BigDecimal toiletPrice;

    private boolean water;

    private BigDecimal waterPrice;
    
    private boolean contactFormActive;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime openTime;

    @JsonFormat(pattern = "HH:mm")
    private LocalTime closeTime;

    //    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "location_stop_type",
            joinColumns = @JoinColumn(name = "location_id"),
            inverseJoinColumns = @JoinColumn(name = "stop_type_id"))
    private Set<StopType> stopTypes;

//    @Transient
//    @JsonProperty("stopTypes")
//    private Set<String> stopTypesRare;

    @ToString.Exclude
    @OneToMany(mappedBy = "location", fetch = FetchType.EAGER)
    private List<Vessel> vessels;

    private String googleId;


    public enum LocationType {
        PORT, RESTAURANT, CHARTER, OTHER
    }
}
