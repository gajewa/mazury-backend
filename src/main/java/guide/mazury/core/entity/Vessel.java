package guide.mazury.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@Entity(name = "vessel")
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Vessel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    private String name;

    private BigDecimal length;

    private BigDecimal width;

    private BigDecimal immersion;

    private BigDecimal pricePerDayMin;

    private BigDecimal pricePerDayMax;

    private BigDecimal deposit;

    private Integer maxCrew;

    private boolean licenceNeeded;

//    @Transient
//    private String charterPlaceSlug;

    @Enumerated(EnumType.STRING)
    private VesselCategory category;

    @JsonIgnoreProperties({"website", "websiteLabel", "email", "phoneNumber", "lat", "lang", "locationLake", "locationCity", "locationStreet", "kind", "description", "type", "depthMin", "depthMax", "pricePerVessel", "pricePerPerson", "bar", "barDistance", "electricity", "electricityPrice", "shower", "showerPrice", "shop", "shopDistance", "toilet", "toiletPrice", "water", "waterPrice", "contactFormActive", "openTime", "closeTime", "stopTypes", "vessels", "googleId"})
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "charter_location_id")
    private Location location;

    public enum VesselCategory {
        SAILBOAT, MOTORBOAT
    }
}
