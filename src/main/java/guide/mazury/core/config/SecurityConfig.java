package guide.mazury.core.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.web.OAuth2LoginAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.filter.ForwardedHeaderFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final Http401UnauthorizedEntryPoint http401UnauthorizedEntryPoint;
    
    @Value("${app.url}")
    private String appUrl;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(new ForwardedHeaderFilter(), OAuth2LoginAuthenticationFilter.class);
        http.addFilter(requestHeaderAuthenticationFilter());
        http.cors();

        http.authorizeRequests()
                .antMatchers("/users/login**", "/users/register", "/users/confirm", "/callback/", "/webjars/**", "/error**", "/users/availableAuths", "/actuator/**").permitAll()
                .antMatchers(HttpMethod.GET, "/locations/**", "/vessels/**").permitAll()
                .antMatchers(HttpMethod.POST, "/locations/{id}/mail").permitAll()
                .antMatchers("/oauth_login", "/loginFailure", "/loginFailure", "/").permitAll()
                .anyRequest().authenticated()
                .and()
                .oauth2Login()
                .defaultSuccessUrl(appUrl, true)
                .failureUrl(appUrl + "?loginFailed=true")
                .loginPage("/users/availableAuths")
                .and()
                .logout()
                .logoutUrl("/");
        http.csrf().disable();
        http.exceptionHandling().authenticationEntryPoint(http401UnauthorizedEntryPoint);
    }


    @Autowired
    public void configureAuthenticationProviders(AuthenticationManagerBuilder auth, TokenAuthenticationProvider tokenAuthenticationProvider) {
        auth.authenticationProvider(tokenAuthenticationProvider);
    }

    private RequestHeaderAuthenticationFilter requestHeaderAuthenticationFilter() throws Exception {
        RequestHeaderAuthenticationFilter filter = new RequestHeaderAuthenticationFilter();
        filter.setPrincipalRequestHeader(HttpHeaders.AUTHORIZATION);
        filter.setAuthenticationManager(authenticationManager());
        filter.setExceptionIfHeaderMissing(false);
        return filter;
    }

}
