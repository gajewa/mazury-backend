package guide.mazury.core.config;

import guide.mazury.core.component.KeyProvider;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Collections;

@Component
@Slf4j
@RequiredArgsConstructor
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final KeyProvider keyProvider;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String authorizationHeader = authentication.getName();

        try {
            Key key = keyProvider.getKey();

            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(key).parseClaimsJws(authorizationHeader);

            log.info("Token = {} is valid.", getTokenForLog(authorizationHeader));

            return new UsernamePasswordAuthenticationToken(claimsJws.getBody().getSubject(), null, Collections.emptyList());
        } catch (UnsupportedJwtException e) {
            log.warn("Provided JSON Web Token = {} doesn't represent supported Claims JWT, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        } catch (MalformedJwtException e) {
            log.warn("Provided JSON Web Token = {} is invalid or has invalid format, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        } catch(SignatureException e) {
            log.warn("Signature validation of provided JSON Web Token = {} has been failed, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        } catch (ExpiredJwtException e) {
            log.warn("Provided JSON Web Token = {} is expired, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        } catch (IllegalArgumentException e) {
            log.warn("Provided JSON Web Token = {} is null, whitespace or empty, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        } catch (Exception e) {
            log.warn("Unable to authenticate user with provided JSON Web Token = {}, unknown exception occur, error message = {}.", getTokenForLog(authorizationHeader), e.getMessage());
            log.debug("JSON Web Token full stack trace.", e);
        }

        return null;
    }

    private String getTokenForLog(String token) {
        return StringUtils.substringAfterLast(token, ".");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return PreAuthenticatedAuthenticationToken.class.isAssignableFrom(authentication);
    }
}

