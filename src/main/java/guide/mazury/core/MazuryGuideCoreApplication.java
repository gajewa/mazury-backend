package guide.mazury.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MazuryGuideCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MazuryGuideCoreApplication.class, args);
	}

}
