package guide.mazury.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GooglePlaceDetailsAddressComponent {

    @JsonProperty("short_name")
    private String name;
    
    private List<String> types;
}
