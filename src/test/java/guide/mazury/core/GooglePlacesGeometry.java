package guide.mazury.core;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;


@Getter
@Setter
public class GooglePlacesGeometry {
    
    private Map<String, String> location;
    
}
