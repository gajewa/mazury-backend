package guide.mazury.core;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GooglePlacesResponse {
    
    List<GooglePlacesResponseResultItem> results;
    
}
