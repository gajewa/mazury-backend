package guide.mazury.core;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.slugify.Slugify;
import guide.mazury.core.entity.Location;
import guide.mazury.core.repository.LocationRepository;
import guide.mazury.core.repository.StopTypeRepository;
import guide.mazury.core.repository.VesselRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@SpringBootTest
class MazuryGuideCoreApplicationTests {

    private static final Pattern NEXT_PAGE_TOKEN_PATTERN = Pattern.compile("\"next_page_token\" : \"(.*?)\"");
//            Pattern.compile("FEE_(.*?)-\\d");

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private StopTypeRepository stopTypeRepository;

    @Autowired
    private VesselRepository vesselRepository;

//    @Test
//    void contextLoads() throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        List<Location> a = mapper.readValue(new File("C:\\repos\\mazury\\mazury\\raredata.json"), new TypeReference<List<Location>>() {
//        });
//        int i = 0;
//        a.stream().forEach(l -> {
//            Set<StopType> stopTypes = CollectionUtils.isEmpty(l.getStopTypesRare()) ? Set.of() : l.getStopTypesRare().stream()
//                    .map(st -> stopTypeRepository.findByName(st.strip().toLowerCase()))
//                    .collect(Collectors.toSet());
//            if(stopTypes.stream().anyMatch(Objects::isNull)){
//                System.out.println("NULL STOP TYPE " + l.toString());
//            }
//            l.setStopTypes(stopTypes);
//            locationRepository.save(l);
//        });
//    }

//    @Test
//    void vessels() throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        List<Vessel> a = mapper.readValue(new File("C:\\repos\\mazury\\mazury\\charterableVesselsrare.json"), new TypeReference<List<Vessel>>() {
//        });
//        int i = 0;
//        a.stream().forEach(l -> {
//            String slug = l.getCharterPlaceSlug();
//            
//            Location lll  = locationRepository.findBySlug(slug).orElseThrow();
//            
//            l.setLocation(lll);
//            vesselRepository.save(l);
//        });
//    }

    @Test
    void charterPlaces() throws IOException {
        String baseUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=czarter&location=54.0002355,21.7323973&radius=50000&key=AIzaSyAnciZKnc2vlUVVY7nCYBstoxa3gbbuerc";
//        String baseUrl = "https://webhook.site/707aaed4-4b42-4f31-a2fc-25424564b780?keyword=czarter&location=54.0002355,21.7323973&radius=50000&key=AIzaSyAnciZKnc2vlUVVY7nCYBstoxa3gbbuerc";

        int i = 1;

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(baseUrl, String.class);
        Files.write(Paths.get("charter-data/" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss-SSSSSSSSS")) + ".txt"), response.getBytes());

        Matcher matcher = NEXT_PAGE_TOKEN_PATTERN.matcher(response);
        while (matcher.find()) {
            i++;
            String nextPageToken = matcher.group(1);
            String nextPageUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyAnciZKnc2vlUVVY7nCYBstoxa3gbbuerc&pagetoken=" + nextPageToken;
            response = restTemplate.getForObject(nextPageUrl, String.class);
            Files.write(Paths.get("charter-data/" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss-SSSSSSSSS")) + ".txt"), response.getBytes());
            log.info("Saving {} file", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss-SSSSSSSSS")));
            matcher = NEXT_PAGE_TOKEN_PATTERN.matcher(response);
        }
    }

    @Test
    void parsePlacesToInserts() throws IOException {
        StringBuilder fullInsert = new StringBuilder("insert into location(name, slug, google_id, lat, lang, type) values ");
        Files.list(Paths.get("charter-data"))
                .forEach(file -> {
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    try {
                        GooglePlacesResponse a = mapper.readValue(new File("charter-data/" + file.getFileName().toString()), GooglePlacesResponse.class);
                        a.getResults().forEach(googlePlacesResponseResultItem -> {
                            fullInsert.append(String.format(
                                    " ('%s', '%s', '%s', '%s', '%s', 'CHARTER'), ",
                                    googlePlacesResponseResultItem.getName(), new Slugify().slugify(googlePlacesResponseResultItem.getName()), googlePlacesResponseResultItem.getPlaceId(), googlePlacesResponseResultItem.getGeometry().getLocation().get("lat"), googlePlacesResponseResultItem.getGeometry().getLocation().get("lng")
                            ));
//                            fullInsert = fullInsert
//                            System.out.println(insert);
                        });
                        int i = 0;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        System.out.println(fullInsert.toString());
    }

    @Test
    void downloadGoogleData() throws IOException {
        locationRepository.findAll().stream()
                .filter(a -> a.getGoogleId() != null)
                .forEach(this::saveLocDetails);
    }

    @SneakyThrows
    private void saveLocDetails(Location loc) {
        String baseUrl = "https://maps.googleapis.com/maps/api/place/details/json" +
//                "?fields=name%2Crating%2Cformatted_phone_number" +
                "?place_id=" + loc.getGoogleId() +
                "&key=AIzaSyAnciZKnc2vlUVVY7nCYBstoxa3gbbuerc";

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(baseUrl, String.class);
        log.info("Saving file {} ", "charter-data/single-location-details/" + loc.getSlug() + ".txt");
        Files.write(Paths.get("charter-data/single-location-details/" + loc.getSlug() + ".txt"), response.getBytes());
    }

    @Test
    void updatePlacesFromDetailsFiles() throws IOException {
        Files.list(Paths.get("charter-data/single-location-details"))
                .forEach(file -> {
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    try {
                        GooglePlaceDetails a = mapper.readValue(new File("charter-data/single-location-details/" + file.getFileName().toString()), GooglePlaceDetails.class);
                        Location location = locationRepository.findByGoogleId(a.getResult().getPlaceId());
                        location.setWebsite(a.getResult().getWebsite());
                        location.setPhoneNumber(a.getResult().getPhoneNumber());
                        location.setLocationCity(getLocationCity(a.getResult()));
                        location.setLocationStreet(getLocationStreet(a.getResult()));
                        location.setWebsiteLabel("Strona WWW");
                        locationRepository.save(location);
//                        int o = 8;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private String getLocationCity(GooglePlaceDetailsResult result) {
        return result.getAddressComponents()
                .stream()
                .filter(a -> a.getTypes().contains("locality"))
                .findFirst()
                .get()
                .getName();
    }

    private String getLocationStreet(GooglePlaceDetailsResult result) {
        if (result.getAddressComponents()
                .stream()
                .anyMatch(a -> a.getTypes().contains("street_number"))) {
            String street = result.getAddressComponents()
                    .stream()
                    .filter(a -> a.getTypes().contains("route"))
                    .findFirst()
                    .get()
                    .getName();

            String number = result.getAddressComponents()
                    .stream()
                    .filter(a -> a.getTypes().contains("street_number"))
                    .findFirst()
                    .get()
                    .getName();
            return street + " " + number;
        }

        try {

            String street = result.getAddressComponents()
                    .stream()
                    .filter(a -> a.getTypes().contains("locality"))
                    .findFirst()
                    .get()
                    .getName();

            String number = result.getAddressComponents()
                    .stream()
                    .filter(a -> a.getTypes().contains("premise"))
                    .findFirst()
                    .get()
                    .getName();

            return street + " " + number;
        } catch (Exception e) {
            return result.getAddressComponents()
                    .stream()
                    .filter(a -> a.getTypes().contains("locality"))
                    .findFirst()
                    .get()
                    .getName();
        }
    }

    @Test
    public void parseBielakCzartery() {
        String toParse = "\n" +
                "\n" +
                "Futura 40 GrandHorizon VIP DISCO\n" +
                "Futura 40 Grand Horizon VIP DISCO – Bielak Czartery – czarter houseboat Mazury Futura 40 w wersji Grand Horizon to Houseboat, czyli jednostka motorowa o najwyższym standardzie mieszkalnym. Jest to jednostka najbardziej wyróżniająca się od innych Futur 40 dostępnych w czarterze z uwagi na zamontowanie profesjonalnego sprzętu grającego zarówno w mesie jak i przy trapie…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Flybridge\n" +
                "Trzy podwójne oddzielnie zamykane kabiny + dwa wygodne miejsca w kokpicie\n" +
                "Trzy łazienki z prysznicami (w każdej kabinie)\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Futura 40 Grand Horizon 2018\n" +
                "Futura 40 Grand Horizon – Bielak Czartery – czarter houseboat Mazury Futura 40 w wersji Grand Horizon to Houseboat, czyli jednostka motorowa o najwyższym standardzie mieszkalnym. Jest to nowsza, większa siostra jachtu Futura 36, który ogromną popularność zyskał już w sezonie 2016, czyli w roku swojej premiery. Futura 40 to najnowszy projekt Przemysława Cieśniarskiego; łączy…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Flybridge\n" +
                "Trzy podwójne oddzielnie zamykane kabiny + dwa wygodne miejsca w kokpicie\n" +
                "Trzy łazienki z prysznicami (w każdej kabinie). Do każdej kabiny oddzielna łazienka\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Futura 36 „Carmelia 2” High Line Pro\n" +
                "Futura 36 – Bielak Czartery – czarter houseboat Mazury Futura 36 w wersji High Line Pro to Houseboat, czyli jednostka motorowa o najwyższym standardzie mieszkalnym. W komfortowych warunkach może na niej nocować osiem osób. Jest to jacht polskiego projektanta Przemysława Cieśniarskiego i znakomicie sprawdza się na akwenach Krainy Wielkich Jezior Mazurskich. Mimo rozmiaru manewrowanie jednostką…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość całkowita: 1100 cm\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Wysokość w kabinie: 195 cm\n" +
                "Zanurzenie : 55 cm\n" +
                "Ilość koi: 3×2 + 2\n" +
                "Trzy podwójne oddzielnie zamykane kabiny + dwa wygodne miejsca w kokpicie\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Nautika 1000 s VIP – „Czarny Piotruś”\n" +
                "Nautika 1000 s VIP – Bielak Czartery – czarter houseboat mazury Nautika 1000 s VIP to Houseboat, czyli jednostka motorowa o najwyższym standardzie mieszkalnym. W komfortowych warunkach może na niej nocować osiem osób. Zaprojektowana została przez polskiego projektanta, pana Andrzeja Skrzata i znakomicie sprawdza się na akwenach Krainy Wielkich Jezior Mazurskich. Mimo rozmiaru manewrowanie jednostką…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość całkowita: 1000 cm\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Wysokość w kabinie: 195 cm\n" +
                "Zanurzenie : 55 cm\n" +
                "Ilość koi: 3×2 + 2\n" +
                "Trzy podwójne oddzielnie zamykane kabiny + dwa wygodne miejsca w mesie\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Suncamper 35 Flybridge\n" +
                "Suncamper 35 to Houseboat, czyli łódź motorowa o najwyższym komforcie zakwaterowania załogi. Jest to największa jednostka rekreacyjna pływająca po mazurskich akwenach z zaburtowym silnikiem. Nowoczesne wykończenia i najwyższej jakości sprzęt nadają jednostce świeży, luksusowy charakter. Łódź dedykowana jest koneserom żeglugi motorowej w ekskluzywnych warunkach; każdy element jachtu zaprojektowany został z myślą o doskonałym dostosowaniu do…\n" +
                "\n" +
                "Długość całkowita – 10,6 m\n" +
                "Szerokość – 3  m\n" +
                "Zanurzenie – 0,65 m\n" +
                "Ilość koi – 2×2 + 2 + 1\n" +
                "Dwie podwójne oddzielnie zamykane kabiny + dwa wygodne miejsca w kokpicie + miejsce do spania dla jednej osoby\n" +
                "Dwie łazienki\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Nexus 870 Revo Prestige\n" +
                "Nexus 870 Revo Prestige – Bielak Czartery – czarter jachtów mazury Nexus to Houseboat przeznaczony do żeglugi turystycznej i spacerowej. W komfortowych warunkach może na nim nocować nawet sześcioosobowa załoga. Łódź Nexus 870 została wyposażona w silnik zaburtowy o mocy 25 KM. Prowadzenie jachtu nie wymaga posiadania patentu, a po przeszkoleniu za jej sterami pewnie…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość całkowita: 870 cm\n" +
                "Maksymalna liczba załogi: 6\n" +
                "Wysokość w kabinie: 186 cm\n" +
                "Zanurzenie : 45 cm\n" +
                "Ilość koi: 3×2\n" +
                "Dwie podwójne koje oddzielnie zamykane koje + rozkładana koja w mesie\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Sea Ray 265 Sundancer\n" +
                "Sea Ray 265 Sundancer to nowoczesny  luksusowy jacht motorowy Ponad osiem metrów długości kadłuba przyniosło przestrzenną kabinę mieszkalną, którą w komfortowych warunkach może zamieszkiwać nawet do 4 osób. Jacht zbudowany jest zgodnie z nowoczesną stylistyką. Pływanie Calipso wymaga uprawnień. Sterowanie jednostką jest dziecinnie proste dzięki mocnemu skrętnemu silnikowi Mercrusier 6.2 l o mocy 350 KM,…\n" +
                "\n" +
                "Długość całkowita: 836 cm\n" +
                "\n" +
                "Zbiornik: 261 litrów\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Wysokość w kabinie: 195 cm\n" +
                "Zanurzenie : 1,2 m\n" +
                "Ilość koi: 2×2\n" +
                "Dwie podwójne koje\n" +
                "\n" +
                " \n" +
                "\n" +
                " \n" +
                "\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "AM 780 Houseboat (dwie kabiny)\n" +
                "AM 780 – Bielak Czartery – czarter jachtów mazury AM 780 jest to łódź motorowa z rodziny Houseboatów, czyli jednostek rekreacyjnych o dużym komforcie mieszkalnym. Na łodzi nocować może nawet czteroosobowa załoga. Prowadzenie jachtu nie wymaga uprawnień; za sterami jednostki po krótkim przeszkoleniu poradzi sobie nawet początkujący sternik. W celu ułatwienia manewrów Houseboat wyposażony jest…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość całkowita: 780 cm\n" +
                "Maksymalna liczba załogi:4 (6)\n" +
                "Wysokość w kabinie: 195 cm\n" +
                "Zanurzenie : 45 cm\n" +
                "Ilość koi: 2×2 (dla 4 osób)\n" +
                "Dwie podwójne oddzielnie zamykane kabiny\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Escapade 600 Exclusive – 150 KM\n" +
                "Escapade 600 Exclusive – 150 KM – Bielak Czartery – czarter motorówek Mazury Escapade 600 Exclusive to nowoczesny, komfortowy jacht kabinowy typu pilothouse, skonstruowany z myślą o akwenach krainy Wielkich Jezior Mazurskich; doskonale sprawdza się zarówno podczas szybkich wypraw sportowych jak i spokojnych rejsów wypoczynkowych. Możemy z niego korzystać w każdych warunkach pogodowych. Jacht wyposażony…\n" +
                "\n" +
                "Rok produkcji: 2017\n" +
                "\n" +
                "Długość: 5.98 m\n" +
                "Szerokość: 2.54 m\n" +
                "Zanurzenie: 0.6 m\n" +
                "Liczba osób: max 6\n" +
                "\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Cobrey 260 SC\n" +
                "Cobrey 260 SC – Bielak Czartery – czarter motorówek Mazury Cobrey 260 SC to szybki jacht motorowy z kabiną umożliwiającą zakwaterowanie załogi. Jacht polskiego projektu, stanowi doskonałe połączenie dopracowanych walorów nautycznych z wysokim standardem mieszkalnym. Niezapomnianych wrażeń, nawet najbardziej wymagającym koneserom rejsów motorowodnych dostarczy silnik Mercruiser o mocy 260 KM; manewrowanie w portach ułatwi dziobowy…\n" +
                "\n" +
                "Długość całkowita: 7,80 m\n" +
                "Szerokość całkowita: 2,59 m\n" +
                "Maksymalne zanurzenie: 0,97 m\n" +
                "Minimalne zanurzenie: 0,46 m\n" +
                "Wysokość kabiny: 1,87 m\n" +
                "Maksymalna liczba załogi: 8\n" +
                "Liczba koi: 4\n" +
                "\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Chaparral 1900 SLC – 150 KM\n" +
                "Chaparral 1900 SLC – 150 KM – Bielak Czartery – czarter motorówek Mazury Chaparral 1900 SLC w wersji OPEN to jeden z najszybszych jachtów motorowych na Mazurach. Amerykańska konstrukcja pomieści nawet ośmioosobową załogę. Jacht wyposażony jest w nowy silnik HONDA V-TEC o mocy 150 KM. Na szczególną uwagę zasługuje wyjątkowo niskie spalanie – nawet 11…\n" +
                "\n" +
                "Silnik HONDA 150 KM V-TEC\n" +
                "Długość 5,65 m\n" +
                "Szerokość 2,65 m\n" +
                "Zbiornik 100 litrów\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Sea Ray 1800 BR – 240 KM\n" +
                "Sea Ray 1800 BR – Bielak Czartery – czarter motorówek Mazury Sea Ray 1800 BR jest jednym z najszybszych jachtów motorowych na Mazurach. Wyposażony w stacjonarny silnik benzynowy Mercriuser 4.3l o mocy 240 KM zapewni niezapomniane wrażenia nawet najbardziej wymagającym Motorowodniakom przy spalaniu ok. 14-20l/h. Sea Ray 1800 BR idealnie nadaje się do uprawiania sportów…\n" +
                "\n" +
                "stacjonarny silnik benzynowy Mercruiser 4.3 l o mocy 240 KM\n" +
                "spalanie ok. 14 l/h\n" +
                "załoga: nawet 8 osób\n" +
                "długość: 6,75m\n" +
                "szerokość: 2,25m\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Maxum 1800 SR3\n" +
                "Maxum 1800 SR3 – Bielak Czartery – czarter motorówek Mazury Maxum 1800 SR3 to niezwykle dynamiczny, otwarto pokładowy jacht motorowy przystosowany do rozwijania dużych prędkości. Jacht może zapewnić rozrywkę nawet ośmioosobowej załodze. Maxum wyposażony jest w benzynowy silnik Mercruiser 3.0L Prędkość osiągana przez motorówkę doskonale nadaje się do uprawiania sportów wodnych. Orczyk mocowany jest na…\n" +
                "\n" +
                "max. 8 osób\n" +
                "długość 6,2 m\n" +
                "szerokość 2,46 m\n" +
                "wysokość 1,10 m\n" +
                "zanurzenie 0,90 m\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Smartfisher Pilot\n" +
                "Smart FIsher 460 PILOT to jacht spacerowo-motorowy, wyposażony w ,oszczędny (ok. 3l/h) silnik Honda 15 KM. Idealna jednostka na wędkarskie wypady\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość 4,60 m\n" +
                "Szerokość 2,10 m\n" +
                "Zanurzenie 0,40 m\n" +
                "Załoga max. 4 osoby\n" +
                "Ilość koi: 2\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Texas 595\n" +
                "Texas 595 – Bielak Czartery – czarter jachtów mazury Texas 595 to nowoczesny jacht kabinowy  o przeznaczeniu rekreacyjnym, idealnie nadający się na rejsy rodzinne i wyprawy wędkarskie. Łódź maksymalnie może pomieścić do 6 osób, natomiast jeśli chodzi o nocleg to zapewnia komfort dla 3 osób – jedna kabina sypialna, z rozkładaną koją. Prowadzenie jachtu nie…\n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Długość całkowita: 595cm\n" +
                "Maksymalna liczba załogi: 6\n" +
                "Wysokość w kabinie: 150 cm\n" +
                "Zanurzenie : 30 cm\n" +
                "Ilość koi:  (dla maksymalnie 3 osób)\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Numo 550 Chill&Fun\n" +
                "Numo 550 – Bielak Czartery – czarter motorówka bez patentu Mazury Numo 550 to ekskluzywny jacht motorowy na maksymalnie 8 osób. Konstrukcja została urządzona w eleganckim stylu. Siedzenia pokryte są jasną srebrną skórą. Motorówka wyposażona została w silnik Mercury 20 KM sterowany za pomocą kierownicy. Prowadzenie motorówki nie wymaga posiadania uprawnień – czarter bez patentu….\n" +
                "\n" +
                " \n" +
                "\n" +
                "BEZ UPRAWNIEŃ STERNIKA\n" +
                "Silnik Suzuki 25 KM\n" +
                "Długość 5,50  m\n" +
                "Szerokość 1,80 m\n" +
                "Zbiornik 80 litrów\n" +
                "Załoga:maks 8 osób\n" +
                "Zobacz szczegóły\n" +
                "\n" +
                "\n" +
                "Venice Holiday\n" +
                "Venice Holiday – Bielak Czartery – czarter motorówka bez patentu Mazury Venice Holiday to wygodna łódź motorowa na maksymalnie 8 osób. Wyposażona została w silnik Mercury 20 KM sterowany za pomocą kierownicy. Prowadzenie motorówki nie wymaga posiadania uprawnień – czarter bez patentu. Po krótkim przeszkoleniu, za kierownicą poradzi sobie nawet początkujący sternik. W naszej flocie…";

        String[] desriptions = toParse.split("Zobacz szczegóły\n" +
                "\n" +
                "\n");
        Arrays.stream(desriptions).forEach(desc -> {
            String[] descPositions = desc.split("\n");
//            System.out.println("name: " + descPositions[0]);

            String dl = "null";
            Matcher matcher = Pattern.compile("Długość całkowita: (.*?) cm").matcher(desc);
            if (matcher.find()) {
//                System.out.println("length: " + matcher.group(1));
                dl = matcher.group(1);
                if (dl.length() == 3) {
                    dl = dl.charAt(0) + "." + dl.charAt(1) + dl.charAt(2);
                } else if (dl.length() == 4) {
                    dl = dl.charAt(0) + "" + dl.charAt(1) + "." + dl.charAt(2) + "" +dl.charAt(3);
                }
            }

            String zaloga = "null";
            matcher = Pattern.compile("Maksymalna liczba załogi: (.*?)\n").matcher(desc);
            if (matcher.find()) {
//                System.out.println("Maksymalna liczba załogi: " + matcher.group(1));
                zaloga = matcher.group(1);
            }

            String zanurz = "null";
            matcher = Pattern.compile("Zanurzenie minimalne: (.*?) cm").matcher(desc);
            if (matcher.find()) {
//                System.out.println("Zanurzenie minimalne: " + matcher.group(1));
                zanurz = "0." + matcher.group(1);
            }

            System.out.println("insert into vessel(type, length, immersion, max_crew, category, charter_location_id, licence_needed) " +
                    "values ('%s', %s, %s, %s, '%s', %s, %s);".formatted(descPositions[0], dl, zanurz, zaloga, "MOTORBOAT", "231", "false"));

        });

    }
}
