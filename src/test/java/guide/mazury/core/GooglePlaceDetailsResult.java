package guide.mazury.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class GooglePlaceDetailsResult {

    @JsonProperty("formatted_phone_number")
    private String phoneNumber;

    @JsonProperty("place_id")
    private String placeId;
    
    private String website;
    
    @JsonProperty("address_components")
    private List<GooglePlaceDetailsAddressComponent> addressComponents;
}
