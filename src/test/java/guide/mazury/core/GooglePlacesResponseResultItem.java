package guide.mazury.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class GooglePlacesResponseResultItem {
    
    private String name;
    
    @JsonProperty("place_id")
    private String placeId;
    
    private GooglePlacesGeometry geometry;
}
