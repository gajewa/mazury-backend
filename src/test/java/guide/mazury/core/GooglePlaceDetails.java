package guide.mazury.core;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GooglePlaceDetails {

    private GooglePlaceDetailsResult result;
    
}
