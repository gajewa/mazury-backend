#FROM adoptopenjdk/openjdk16:ubi
#RUN mkdir /opt/app
#COPY target/*.jar /opt/app/app.jar
#CMD ["java", "-jar", "/opt/app/app.jar"]

FROM adoptopenjdk/openjdk16:ubi as build
WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN chmod +x ./mvnw
RUN ./mvnw install -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)

FROM adoptopenjdk/openjdk16:ubi
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
RUN ls
ENTRYPOINT ["java","-cp","app:app/lib/*","guide.mazury.core.MazuryGuideCoreApplication"]